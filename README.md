# adbfs

Mount your android file system through adb

# Quickstart
```console
$ make
$ ./out mount_point
```
 and the root directory of the android filesystem will be mounted onto `mount_point`
