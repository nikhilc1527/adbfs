# CFLAGS = `pkg-config fuse3 --cflags --libs` -Wall -Wextra -pedantic -ggdb -g
CFLAGS = -Wall -Wextra -pedantic -ggdb -g
LDFLAGS = -lfuse3 -lpthread
INCFLAGS = -I/usr/include/fuse3
OUTPUT = out

all: $(OUTPUT) run

$(OUTPUT): main.c
	gcc $(CFLAGS) $(LDFLAGS) $(INCFLAGS) main.c -o $(OUTPUT)

run: $(OUTPUT)
	rm /tmp/adbfs_log.txt || true
	fusermount -q -u -z dir || true
	./out dir
