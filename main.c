#define FUSE_USE_VERSION 31
#include <fuse.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <assert.h>

void log_info(const char* msg) {
  FILE* log_file = fopen("/tmp/adbfs_log.txt", "a");
  fprintf(log_file, "[INFO]: %s\n", msg);
  fflush(log_file);
  fclose(log_file);
}

void log_command(char *const *command) {
  int cmd_len = 0;
  while (command[cmd_len] != NULL) cmd_len++;

  assert(cmd_len > 0);

  int total_len = strlen(command[0]);

  for (int i = 1; i < cmd_len; ++i) {
    total_len++;
    total_len += strlen(command[i]);
  }

  assert(total_len > 0);

  char *str = (char*)malloc(total_len + 1);
  str[total_len] = '\0';

  int j = 0;
  int cur_len = strlen(command[0]);
  memcpy(str + j, command[0], cur_len);
  j = cur_len;
  
  for (int i = 1; i < cmd_len; ++i) {
    str[j] = ' ';
    j++;
    cur_len = strlen(command[i]);
    memcpy(str + j, command[i], cur_len);
    j += cur_len;
  }

  FILE* log_file = fopen("/tmp/adbfs_log.txt", "a");
  fprintf(log_file, "[CMD]: %s\n", str);
  fflush(log_file);
  fclose(log_file);
  free(str);
}

char* call_process(const char *filepath, char *const *command) {
  log_command(command);
  
  int pipefd[2];
  int res = pipe(pipefd);
  if (res < 0) {
    fprintf(stderr, "could not create pipes because of %s", strerror(errno));
    return NULL;
  }
  int readfd = pipefd[0];
  int writefd = pipefd[1];

  int forked = fork();
  if (forked < 0) {
    fprintf(stderr, "could not fork process because of %s", strerror(errno));
    return NULL;
  } else if (forked == 0) {
    // child process
    close(readfd);
    
    dup2(writefd, 1);
    dup2(writefd, 2);

    execvp(filepath, command);

    return NULL;
  } else {
    // parent process
    waitpid(forked, NULL, 0);
    close(writefd);
    const int bufsize = 1024;
    char buffer[bufsize];
    int readed = read(readfd, buffer, bufsize);
    char *res_str = malloc(bufsize+1);
    int cursize = 0;
    res_str[cursize] = '\0';
    while (readed > 0) {
      char *new_res = malloc(cursize + readed + 1);
      memcpy(new_res, res_str, cursize);
      memcpy(new_res + cursize, buffer, readed);
      free(res_str);
      res_str = new_res;
      cursize += readed;
      res_str[cursize] = '\0';
      readed = read(readfd, buffer, bufsize);
    }
    return res_str;
  }
}

void* adbfs_init(struct fuse_conn_info *conn,
			struct fuse_config *cfg) {
  log_info("init adbfs");
  cfg->kernel_cache = 1;
  return NULL;
}

int adbfs_getattr(const char *path, struct stat *stbuf, struct fuse_file_info *file_info) {
  char *filetype;
  {
    char *cmd[] = {"adb", "shell", "stat", "-c", "%F", path, NULL};
    filetype = call_process("adb", cmd);
    /* filetype[strlen(filetype) - 1] = '\0'; */
  }

  char *permissions;
  {
    char *cmd[] = {"adb", "shell", "stat", "-c", "%a", path, NULL};
    permissions = call_process("adb", cmd);
    /* permissions[strlen(permissions) - 1] = '\0'; */
  }
  
  char *size_str;
  {
    char *cmd[] = {"adb", "shell", "stat", "-c", "%s", path, NULL};
    size_str = call_process("adb", cmd);
    /* log_info("size of file:"); */ 
    /* log_info(size_str); */
    /* filesize = atoi(size_str); */
    /* free(size_str); */
  }

  log_info("getting attributes of:");
  log_info(path);
  log_info(filetype);
  log_info(permissions);
  log_info(size_str);
  log_info("done printing attributes");

  int res = 0;
  if (!strcmp(filetype, "directory\n") || !strcmp(filetype, "symbolic link\n")) {
    stbuf->st_mode = S_IFDIR | 0777;
  } else if (!strcmp(filetype, "regular file\n")) {
    stbuf->st_mode = S_IFREG | 0777;
  } else {
    res = 0;
  }
  stbuf->st_nlink = 0;
  stbuf->st_size = atoi(size_str);

  free(filetype);
  free(permissions);
  free(size_str);
  
  return res;
}

int adbfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
			 off_t offset, struct fuse_file_info *fi,
			 enum fuse_readdir_flags flags) {
  (void)offset;
  (void)fi;
  (void)flags;

  log_info("reading directory");
  log_info(path);

  /* if (strcmp(path, "/") != 0) */
  /*   return -ENOENT; */

  filler(buf, ".", NULL, 0, 0);
  filler(buf, "..", NULL, 0, 0);

  char *ls;
  int len;
  {
    char *args[] = {"abd", "shell", "ls", path, NULL};
    ls = call_process("adb", args);
    len = strlen(ls);
  }

  int i = 0;
  for (int j = 0; j < len; ++j) {
    if (ls[j] == '\n') {
      ls[j] = '\0';
      log_info("found file/directory: ");
      log_info(ls + i);
      filler(buf, ls + i, NULL, 0, 0);
      i = j + 1;
    }
  }

  return 0;
}

static int adbfs_open(const char *path, struct fuse_file_info *fi) {
  /* if (strcmp(path + 1, options.filename) != 0) */
  /*   return -ENOENT; */

  if ((fi->flags & O_ACCMODE) != O_RDONLY)
    return -EACCES;

  return 0;
}

static int adbfs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
  log_info("reading");
  log_info(path);
  
  size_t len;
  (void)fi;

  char* contents;
  int content_len;
  {
    char inputfile[300];
    sprintf(inputfile, "if=%s", path);
    char count[20];
    sprintf(count, "count=%d", size);
    char skip[20];
    sprintf(skip, "skip=%d", offset);
    char* args[] = {"adb", "shell", "dd", "status=none", inputfile, "iflag=count_bytes,skip_bytes", count, skip, NULL};
    contents = call_process("adb", args);
    content_len = strlen(contents);
  }

  if (content_len > size) {
    size = content_len;
  }

  memcpy(buf, contents, size);

  return size;
}

int check_adb_connection() {
  log_info("checking adb connection");
  char* adb_args[] = {"adb", "get-state", NULL};
  char *adb_output = call_process("adb", adb_args);
  int output_len = strlen(adb_output);
  int device_ind = 0;

  int res = !strcmp(adb_output, "device\n");
  free(adb_output);
  return res;
}

int main2(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "need at least one argument to print the file\n");
    return 1;
  }
  char* args[] = {"cat", argv[1], NULL};
  char *res = call_process("cat", args);
  printf("%s\n", res);
  free(res);
  
  return 0;
}

int main(int argc, char *argv[]) {
  if (argc < 2) {
    fprintf(stderr, "need to give the directory to mount adbfs\n");
    return 1;
  }

  if (!check_adb_connection()) {
    fprintf(stderr, "could not find device connected. please connect adb device and try again\n");
    return 1;
  } else {
    printf("found adb connection with device\n");
  }

  char *dir_path = argv[1];
  int create = 0;
  if (argc > 2 && !(strcmp(argv[2], "-c")))
    create = 1;

  DIR *dir = opendir(dir_path);
  if (!dir) {
    char info[300];
    sprintf(info, "directory %s does not exist", dir_path);
    log_info(info);
    if (create) {
      char* mkdir_args[] = {"mkdir", dir_path, NULL};
      free(call_process("mkdir", mkdir_args));
    }
  } else {
    printf("found directory %s\n", dir_path);
  }

  struct fuse_operations adbfs_ops = {
    .init = adbfs_init,
    .getattr = adbfs_getattr,
    .readdir = adbfs_readdir,
    .open = adbfs_open,
    .read = adbfs_read,
  };

  close(open("/tmp/adbfs_log.txt", O_TRUNC));
  log_info("starting fuse_main");
  
  int res = fuse_main(argc, argv, &adbfs_ops, NULL);

  return res;
}
